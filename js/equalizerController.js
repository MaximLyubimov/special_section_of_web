//this.equalizerView.onTimerTickEvent=this.renderEqualizer.bind(this);

var Controller =function(View,Model){
    this.equalizerView=View;
    this.equalizerModel=Model;
}
Controller.prototype.init=function()
{ 
    this.equalizerView.createLanes(this.equalizerModel.getNumrerOfLines());

    this.equalizerView.startClickEvent=this.startEqualirer.bind(this);
    this.equalizerView.stopClickEvent=this.stopEqualizer.bind(this);
    this.equalizerView.EventIncreaseAnimationTime=this.setAnimationTime.bind(this,50);
    this.equalizerView.EventDecreaseAnimationTime=this.setAnimationTime.bind(this,-50);
    for(var i=0;i<this.equalizerModel.getNumrerOfLines();i++)
        this.equalizerView.changeColorEvent.push(this.getColor.bind(this,i));
    this.equalizerView.init();
    
    this.equalizerView.initLanesListener();    
}
Controller.prototype.renderEqualirer=function()
{
    this.equalizerView.render(this.equalizerModel.randomHeights(),this.equalizerModel.getAnimationTime());
}
Controller.prototype.getColor=function(i)
{
    if (i==-1)
        return this.equalizerModel.getRandomColor();
    this.equalizerView.changeColor(i,this.equalizerModel.getRandomColor());
   
}
Controller.prototype.setAnimationTime=function(time)
{
    this.equalizerModel.setAnimationTime(this.equalizerModel.getAnimationTime()-time);
    this.equalizerView.setAnimationTime(this.equalizerModel.getAnimationTime());
    clearInterval(this.equalizerView.intervalId);
    this.equalizerView.onTimerTickEvent=this.renderEqualirer.bind(this);
    this.equalizerView.intervalId=setInterval(this.equalizerView.onTimerTickEvent,this.equalizerModel.getAnimationTime());
    this.equalizerView.setSpeedField(this.equalizerModel.getAnimationTime());
}

Controller.prototype.startEqualirer=function()
{
    if (this.equalizerView.onTimerTickEvent!==null)
        return;
    this.equalizerView.onTimerTickEvent=this.renderEqualirer.bind(this);
    this.equalizerView.setAnimationTime(this.equalizerModel.getAnimationTime());
    this.equalizerView.intervalId=setInterval(this.equalizerView.onTimerTickEvent,this.equalizerModel.getAnimationTime());
    this.equalizerView.setSpeedField(this.equalizerModel.getAnimationTime());
   
}
Controller.prototype.stopEqualizer=function()
{   
    if (this.equalizerView.onTimerTickEvent==null)
        return;
    clearInterval(this.equalizerView.intervalId);
    this.equalizerView.onTimerTickEvent=null;
}

var equalizerController= new Controller(equalizerView,equalizerModel);
equalizerController.init();
