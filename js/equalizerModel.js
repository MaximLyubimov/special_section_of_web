
const MIN_HEIGHT=10;
const MAX_HEIGHT=300;
const NUMBER_LANES=32;
const ANIMATION_TIME=300;
var Model=function(){
    this.objs={
 'equalizer': {
    min_height: MIN_HEIGHT,
    max_height: MAX_HEIGHT,
    number_lanes:NUMBER_LANES,
    animation_time:ANIMATION_TIME
    }
 };
};
Model.prototype.getAnimationTime= function()
{
    return equalizerModel.objs.equalizer.animation_time;
}
Model.prototype.getNumrerOfLines=function()
{
    return equalizerModel.objs.equalizer.number_lanes;
}
Model.prototype.getRandomColor=function()
{
   return [Math.random()*(220-20)+20,Math.random()*(220-20)+20,Math.random()*(220-20)+20];
}
Model.prototype.randomHeights=function()
{
    var MasHeights=new Array();
    for (var i=0;i<equalizerModel.objs.equalizer.number_lanes;i++)
    {
        MasHeights.push(parseInt(Math.random() * (equalizerModel.objs.equalizer.max_height - equalizerModel.objs.equalizer.min_height) + equalizerModel.objs.equalizer.min_height));
    }
    return MasHeights;

}
Model.prototype.setAnimationTime=function(time)
{
    if (time>=50)
         equalizerModel.objs.equalizer.animation_time=time;
}
var equalizerModel=new Model();