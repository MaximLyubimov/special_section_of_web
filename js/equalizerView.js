var View = function()
{
    this.MasLanes=document.getElementsByClassName("Lane");
    this.onTimerTickEvent=null;
    this.buttonStart=document.getElementById("StartEqualizer");
    this.buttonStop=document.getElementById("StopEqualizer");
    this.startClickEvent=null;
    this.stopClickEvent=null;
    this.intervalId=null;
    this.buttonIncrease=document.getElementById("IncreaseSpeed");
    this.decreaseAnimationTimeEvent=null;
    this.increaseAnimationTimeEvent=null;
    this.buttonDecrease=document.getElementById("DecreaseSpeed");
    this.changeSpeedValueEvent=null;
    this.fieldForSpeed=document.getElementById("CurrentSpeed");
    this.changeColorEvent=new Array();
    this.MasLanesHeightLast=new Array(40).fill(130);
    this.canvas=document.getElementById("Container");
    this.Colors=new Array();
    //this.obj=new Array(32).fill(130);
}

View.prototype.init=function()
{
    this.buttonStart.addEventListener("click", this.startClickEvent);
    this.buttonStop.addEventListener("click", this.stopClickEvent);
    this.buttonIncrease.addEventListener("click",this.EventIncreaseAnimationTime);
    this.buttonDecrease.addEventListener("click",this.EventDecreaseAnimationTime); 
    for (var i = 0; i < this.MasLanes.length; i++) {
        this.MasLanes[i].addEventListener('click', this.changeColorEvent[i]);
       
    }
    
    this.canvas.addEventListener("click",this.changeColor);
}
View.prototype.createLanes=function(numberLanes)
{
    var c=document.getElementById("Container");
    var ctx=c.getContext("2d");
    for (var i=0;i<numberLanes;i++)
    {
        ctx.fillStyle="#FF0000";
        ctx.fillRect(4+(i*24),130,20,220);
        this.MasLanes[i]=4+(i*24);
        this.Colors.push("rgb(255,0,0)");
        
    }


}
View.prototype.changeColor=function(i,Color)
{

    Color=equalizerController.getColor(-1);
    startX=i.clientX-i.offsetX;
    strtY=i.clientY-i.offsetY;
    i=parseInt(i.offsetX/24);
    equalizerView.Colors[i] ="rgb("+Color[0]+","+Color[1]+","+Color[2]+")";
    equalizerView.render(equalizerView.MasLanesHeightLast,1);

}
View.prototype.setSpeedField=function(time)
{
    this.fieldForSpeed.innerText=time+" ms";
}

View.prototype.setAnimationTime = function(time)
{
  
}
View.prototype.initLanesListener=function()
{


}
View.prototype.drawRects=function(objPart)
{
    var c=document.getElementById("Container");
    var ctx=c.getContext("2d");
    ctx.clearRect(0, 0, c.width, c.height);
    for (var i=0;i<objPart.length;i++)
    {
        ctx.fillStyle=this.Colors[i];
        ctx.fillRect(4+(i*24),objPart[i],20,350-objPart[i]);
            
    }

}

  function clone(obj) {
    if (null == obj || "object" != typeof obj) return obj;
    var copy = obj.constructor();
    for (var attr in obj) {
        if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
    }
    return copy;
}
function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }
View.prototype.render= async function(obj,time)
{
   
    if (this.MasLanesHeightLast[0]==null)
        this.MasLanesHeightLast=obj;
    var times=time/20;
    var objPart=clone(obj);
    var difference=new Array();
    for (var j=0;j<obj.length;j++)
        {
            difference[j]=Math.abs(this.MasLanesHeightLast[j]-obj[j])/times;
        }
    for (var i=times;i>0;i--)
    {
        for (var j=0;j<obj.length;j++)
        { 
            if (obj[j]>this.MasLanesHeightLast[j])
                objPart[j]=this.MasLanesHeightLast[j]+(difference[j]);
            else 
                objPart[j]=this.MasLanesHeightLast[j]-(difference[j]);
        }
        this.drawRects(objPart);
        await sleep(10);
        this.MasLanesHeightLast=objPart;  
    }

    
    
    
    
}
var equalizerView=new View();